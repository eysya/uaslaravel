<?php

use Illuminate\Database\Seeder;

class PekerjaanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pekerjaan')->insert(array(
            array(
                'id_pekerjaan'   => '001',
                'nama_pekerjaan'  => 'wiraswasta',
                'created_at'    => now(),
            ),
            array(
                'id_pekerjaan'   => '002',
                'nama_pekerjaan'  => 'Pengusaha',
                'created_at'    => now(),
            ),
            array(
                'id_pekerjaan'   => '003',
                'nama_pekerjaan'  => 'Pegawai Negeri Sipil',
                'created_at'    => now(),
            ),
        ));
    }
}
