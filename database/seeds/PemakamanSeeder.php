<?php

use Illuminate\Database\Seeder;

class PemakamanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('mendiang')->insert(array(
            array(
                'id_pemakaman'   => '001',
                'id_makam'  => '001',
                'nama_mendiang'   => 'jumanji',
                'Bin_Binti'    => 'wahidin',
                'jenis_kelamin'   => 'laki-laki',
                'tempat'   => 'bandung',
                'tanggal_lahir'   => now(),
                'tanggal_meninggal'   => now(),
                'tanggal_dimakamkan'   => now(),
                'jalan_gang'    => 'merak',
                'nomor_rumah'    => '15',
                'RT'    => 'foto ktp',
                'RW'    => 'foto ktp',
                'id_village'    => '1',
                'id_districts'    => '1',
                'id_city'    => '1',
                'fc_ktp'    => 'foto ktp',
                'surat_kematian'    => 'surat',
                'surat_pengantar'    => 'surat pengantar',
                'lokasi'    => '1',
                'blok'    => '1',
                'nomor'    => '1',
                'kelas'    => '1',
                'id_ahli_waris'   => '001',
                'is_active'     => 1,
                'created_at'    => now(),
            ),
        ));
    }
}
