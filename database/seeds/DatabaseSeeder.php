<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(Mastersieeder::class);
        $this->call(BiayaSeeder::class);
        $this->call(PekerjaanSeeder::class);
        $this->call(CitySeeder::class);
        $this->call(DistrictsSeeder::class);
        $this->call(villageSeeder::class);
        $this->call(AhliwarisSeeder::class);
        $this->call(PemakamanSeeder::class);
        $this->call(usersSeeder::class);
    }
}
