<?php

use Illuminate\Database\Seeder;

class BiayaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('biaya_pemakaman')->insert(array(
            array(
                'kode_biaya'   => '001',
                'makam_id'  => '001',
                'biaya'   =>1200000,
                'per_tahun'    => 20000,
                'is_active'     => 1,
                'created_at'    => now(),
            ),
            array(
                'kode_biaya'   => '002',
                'makam_id'  => '002',
                'biaya'   => 1200000,
                'per_tahun'    => 20000,
                'is_active'     => 1,
                'created_at'    => now(),
            ),
        ));
    }
}
