<?php

use Illuminate\Database\Seeder;

class DistrictsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('districts')->insert(array(
            array(
                'id_districts'   => '001',
                'id_city'   => '001',
                'nama'  => 'cibiru Wetan ',
                'created_at'    => now(),
            ),
        ));
    }
}
