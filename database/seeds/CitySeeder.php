<?php

use Illuminate\Database\Seeder;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('city')->insert(array(
            array(
                'id_city'   => '001',
                'nama'  => 'Bandung',
                'created_at'    => now(),
            ),
        ));
    }
}
