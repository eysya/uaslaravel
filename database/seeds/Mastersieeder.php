<?php

use Illuminate\Database\Seeder;

class Mastersieeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('master_makam')->insert(array(
            array(
                'id_makam'   => '001',
                'no_makam'  => '001',
                'kode_blok'   => 'A',
                'lokasi'    => 'Sirnaraga',
                'kelas'   => 'Suplier',
                'is_active'     => 1,
                'created_at'    => now(),
            ),
            array(
                'id_makam'   => '002',
                'no_makam'  => '002',
                'kode_blok'  => 'A',
                'lokasi'    => 'Sirnaraga',
                'kelas'   => 'Vip',
                'is_active'     => 1,
                'created_at'    => now(),
            ),
        ));
    }
}
