<?php

use Illuminate\Database\Seeder;

class AhliwarisSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ahli_waris')->insert(array(
            array(
                'id_ahli_waris'   => '001',
                'nama_ahli_waris'  => 'Junaidin',
                'umur'   => '78',
                'telepon'   => '085216088833',
                'id_pekerjaan'    => '003',
                'jalan_gang'   => 'mekar',
                'nomor'   => '17',
                'RT'   => '007',
                'RW'   => '011',
                'id_city'   => '001',
                'id_districts'   => '001',
                'id_village'   => '001',
                'is_active'     => 1,
                'created_at'    => now(),
            ),
        ));
    }
}
