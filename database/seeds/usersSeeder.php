<?php

use Illuminate\Database\Seeder;

class usersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(array(
            array(
                'id'   => '1',
                'Username'  => 'Manan',
                'email' => 'manan@email.com',
                'password'=>bcrypt('1234567'),
                'phone'=> '085216088833',
                'is_active'     => 1,
                'created_at'    => now(),
            ),
        ));
    }
}
