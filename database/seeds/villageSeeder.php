<?php

use Illuminate\Database\Seeder;

class villageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('village')->insert(array(
            array(
                'id_village'   => '001',
                'nama'  => 'Cibiru ',
                'id_city'  => '001 ',
                'id_districts'  => '001',
                'created_at'    => now(),
            ),
        ));
    }
}
