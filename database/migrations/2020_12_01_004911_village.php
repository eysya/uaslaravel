<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Village extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('village', function (Blueprint $table) {
            $table->bigIncrements('id_village');
            $table->string('nama');
            $table->bigInteger('id_city')->unsigned();
            $table->bigInteger('id_districts')->unsigned();
            $table->timestamps();
            $table->foreign('id_city')->references('id_city')->on('city');
            $table->foreign('id_districts')->references('id_districts')->on('districts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('village');
    }
}
