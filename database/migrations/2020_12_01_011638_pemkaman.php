<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Pemkaman extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mendiang', function (Blueprint $table) {
            $table->bigIncrements('id_pemakaman');
            $table->bigInteger('id_makam')->unsigned();
            $table->string('nama_mendiang');
            $table->string('Bin_Binti');
            $table->string('jenis_kelamin');
            $table->string('tempat');
            $table->date('tanggal_lahir');
            $table->date('tanggal_meninggal');
            $table->date('tanggal_dimakamkan');
            $table->string('jalan_gang');
            $table->integer('nomor_rumah');
            $table->string('RT');
            $table->string('RW');
            $table->bigInteger('id_village')->unsigned();
            $table->bigInteger('id_districts')->unsigned();
            $table->bigInteger('id_city')->unsigned();
            $table->string('fc_ktp');
            $table->string('surat_kematian');
            $table->string('surat_pengantar');    
            $table->bigInteger('lokasi')->unsigned();
            $table->bigInteger('blok')->unsigned();
            $table->bigInteger('nomor')->unsigned();
            $table->bigInteger('kelas')->unsigned();
            $table->bigInteger('id_ahli_waris')->unsigned();
            $table->tinyinteger('is_active')->nullable()->default(0);
            $table->timestamps();
            $table->foreign('id_village')->references('id_village')->on('village');
            $table->foreign('id_districts')->references('id_districts')->on('districts');
            $table->foreign('id_city')->references('id_city')->on('city');
            $table->foreign('id_makam')->references('id_makam')->on('master_makam');
            $table->foreign('lokasi')->references('id_makam')->on('master_makam');
            $table->foreign('blok')->references('id_makam')->on('master_makam');
            $table->foreign('nomor')->references('id_makam')->on('master_makam');
            $table->foreign('kelas')->references('id_makam')->on('master_makam');
            $table->foreign('id_ahli_waris')->references('id_ahli_waris')->on('ahli_waris');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pemakaman');
    }
}
