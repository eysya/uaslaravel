<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AhliWaris extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ahli_waris', function (Blueprint $table) {
            $table->bigIncrements('id_ahli_waris');
            $table->string('nama_ahli_waris');
            $table->integer('umur');
            $table->string('telepon');
            $table->bigInteger('id_pekerjaan')->unsigned();
            $table->string('jalan_gang');
            $table->integer('nomor');
            $table->string('RT');
            $table->string('RW');
            $table->bigInteger('id_city')->unsigned();
            $table->bigInteger('id_districts')->unsigned();
            $table->bigInteger('id_village')->unsigned();
            $table->tinyinteger('is_active')->nullable()->default(0);
            $table->timestamps();
            $table->foreign('id_pekerjaan')->references('id_pekerjaan')->on('pekerjaan');
            $table->foreign('id_city')->references('id_city')->on('city');
            $table->foreign('id_districts')->references('id_districts')->on('districts');
            $table->foreign('id_village')->references('id_village')->on('village');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ahli_waris');
    }
}
