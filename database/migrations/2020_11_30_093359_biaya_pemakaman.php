<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BiayaPemakaman extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('biaya_pemakaman', function (Blueprint $table) {
            $table->bigIncrements('kode_biaya');
            $table->bigInteger('makam_id')->unsigned();
            $table->double('biaya');
            $table->double('per_tahun');
            $table->tinyinteger('is_active')->nullable()->default(0);
            $table->timestamps();
            $table->foreign('makam_id')->references('id_makam')->on('master_makam');
 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('biaya_pemakaman');
    }
}
