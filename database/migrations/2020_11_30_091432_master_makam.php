<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MasterMakam extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_makam', function (Blueprint $table) {
            $table->bigIncrements('id_makam');
            $table->string('no_makam');
            $table->string('kode_blok');
            $table->string('lokasi');
            $table->string('kelas');
            $table->tinyinteger('is_active')->nullable()->default(0);
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_makam');
    }
}
