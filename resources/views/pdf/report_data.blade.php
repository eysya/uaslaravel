<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Print Data</title>
    <link rel="stylesheet" href="" media="all" />
    <style>
        @font-face {
            font-family: SourceSansPro;
            src: url(SourceSansPro-Regular.ttf);
            font-family: Scheherazade;
            src: url('../assets/fonts/Scheherazade-Regular.ttf');
        }

        .clearfix:after {
            content: "";
            display: table;
            clear: both;
        }

        a {
            color: #0087C3;
            text-decoration: none;
        }

        body {
            position: relative;
            width: 100%;
            height: 100%;
            margin: 0 auto;
            color: #555555;
            background: #FFFFFF;
            font-size: 14px;
            font-family: SourceSansPro, Scheherazade;
        }

        header {
            padding: 5px 0 0;
            margin-bottom: 15px;
            border-bottom: 1px solid #AAAAAA;
        }

        #logo {
			display:inline-block;
            float: left;
			
        }

        #logo img {
            height: 100px;
        }

        #company {


            text-align: center;
        }


        #details {
            margin-bottom: 10px;
        }

        #client {
            padding-left: 6px;
            text-align: center;
        }

        #client .to {
            color: #777777;
        }

        h2.name {
            font-size: 1.4em;
            font-weight: normal;
            margin: 0;
        }

        h3.desc {
            font-size: 1.1em;
            font-weight: normal;
            color: #000000;
            margin-top: -10px;
        }

        #report {
            display: inline-block;
            text-align: center;
            margin-top: 10px;
        }

        #data {
            display: inline-block;
            text-align: left;
            margin-top: -10px;
        }

        #data p {
            color: #000000;
            font-size: 1em;
            line-height: 1em;
            font-weight: normal;
            margin: 0 0 -10px 0;
        }

        #report h1 {
            text-decoration: underline;
            color: #000000;
            font-size: 1.5em;
            line-height: 1em;
            font-weight: normal;
            margin: 0 0 10px 0;
        }

        #report .date {
            font-size: 1.1em;
            color: #777777;
        }

        table {
            width: 100%;
            border-collapse: collapse;
            border-spacing: 0;
        }

        table th,
        table td {
            padding: 2px;
            /* background: #EEEEEE; */
            text-align: center;
            /* border-bottom: 1px solid #FFFFFF; */
        }

        table th {
            white-space: nowrap;
            font-weight: normal;
        }

        table td {
            text-align: right;
        }


        table td h3 {
            color: #57B223;
            font-size: 1.2em;
            font-weight: normal;
            margin: 0 0 0.2em 0;
        }

        table .no {
            color: #FFFFFF;
            font-size: 1.6em;
            background: #57B223;
        }

        table .desc {
            text-align: left;
        }

        table .unit {
            background: #DDDDDD;
        }

        table .qty {}

        table .total {
            background: #57B223;
            color: #FFFFFF;
        }

        table td.unit,
        table td.qty,
        table td.total {
            font-size: 1.2em;
        }

        table tbody tr:last-child td {
            border: none;
        }

        #left {
            text-align: left;
        }


        #terbilang {
            margin-top: 3px;
            font-style: Bold, Italic;
        }

        #terimkasih {
            margin-top: 5px;
            padding: 5px;
            color: #000000;
            font-size: 1em;
        }

        #thanks {
            font-size: 2em;
            margin-bottom: 50px;
        }

        #notices {
            padding-left: 6px;
            border-left: 6px solid #0087C3;
            font-family: Scheherazade;
            font-style: Italic;
        }

        #notices .notice {
            font-size: 1em;
        }

        footer {
            color: #777777;
            width: 100%;
            height: 30px;
            position: absolute;
            bottom: 0;
            border-top: 1px solid #AAAAAA;
            padding: 8px 0;
            text-align: center;
        }
		#invoice {
			display: inline-block;
			text-align: center;
            margin-right: 150px;
			float: right;
		}

    </style>
</head>

<body>
    <header class="clearfix">
       
        <div id="company">
		<div id="logo">
            <img src="">
        </div>
            <div><h2>PEMERINTAHAN KOTA BANDUNG</h2></div>
            <div><h> DINAS PENATAAN RUANG</h3></div>
            <div><h5>JALAN PANDU NO.32 TELP.(022)6021836 BANDUNG</h5> </div>
            <div><a href=""></a></div>
        </div>
    </header>
    <main > 
        <div id="details" class="clearfix">
            <div id="report">
                <h1>DATA YANG MENINGGAL</h1>
                <h3 class="desc"></h3>
            </div>
        </div>
        <div id="details" class="clearfix">
            <div id="data">
                <p>
                </p><br>
                <table>
                    <tbody>
                        <tr>
                            <th width="50%" style="text-align: left; color: black">Nama</td>
                            <td width="1%" style="text-align: left; color: black">:</td>
                            <td width="200%" style="text-align: left; color:black"value="{{$data->nama_mendiang}}">{{$data->nama_mendiang}}</td>
                        </tr>
                        <tr>
                            <td width="25%" style="text-align: left;color: black">jenis Kelamin</td>
                            <td width="2%" style="text-align: left;color: black">:</td>
                            <td style="text-align: left; color:black"value="{{$data->jenis_kelamin}}">{{$data->jenis_kelamin}}</td>
                        </tr>
                        <tr>
                            <td width="25%" style="text-align: left;color: black">Tanggal Meninggal</td>
                            <td width="2%" style="text-align: left;color: black">:</td>
                            <td style="text-align: left; color:black"value="{{$data->tanggal_meninggal}}">{{$data->tanggal_meninggal}}</td>
                        </tr>
                        <tr>
                            <td width="25%" style="text-align: left;color: black">Tanggal Pemakaman</td>
                            <td width="2%" style="text-align: left;color: black">:</td>
                            <td style="text-align: left; color:black"value="{{$data->tanggal_dimakamkan}}">{{$data->tanggal_dimakamkan}}</td>
                        </tr>
                        <tr>
                            <td width="100%" style="text-align: left;color: black">Lokasi Pemakaman </td>
                            <td width="25%" style="text-align: left;color: black">TPU</td>
                            <td width="2%" style="text-align: left;color: black">:</td>
                            <td width="250px" style="text-align: left; color:black"value="{{$data->havemaster->lokasi}}">{{$data->havemaster->lokasi}}</td>
                           </tr>
                        <tr> 
                        <td></td>
                        <td width="25%" style="text-align: left;color: black">Blok</td>
                            <td width="2%" style="text-align: left;color: black">:</td>
                            <td style="text-align: left; color:black"value="{{$data->havemaster->kode_blok}}">{{$data->havemaster->kode_blok}}</td>

                        </td>
                        </tr>
                        <td></td>
                        <td width="25%" style="text-align: left;color: black">Kelas</td>
                            <td width="2%" style="text-align: left;color: black">:</td>
                            <td style="text-align: left; color:black"value="{{$data->havemaster->kelas}}">{{$data->havemaster->kelas}}</td>
             
                        <tr>
                        </tr>
                        <tr>
                        <td></td>
                        <td width="25%" style="text-align: left;color: black">Nomor</td>
                            <td width="2%" style="text-align: left;color: black">:</td>
							<td width="2%" style="text-align: left;color: black"value="{{$data->havemaster->no_makam}}">{{$data->havemaster->no_makam}}</td>
 

                        </tr>

                    </tbody>
                </table><br>
                <p>
            </div>
        </div>
        <div id="data">
		<div id="report">
                <h1>AHLI WARIS/KELUARGA/PENANGGUNG JAWAB:</h1>
                <h3 class="desc"></h3>
            </div>
            <br><br><br>
            <table>
                <tbody>
                    <tr>
                        <th width="50%" style="text-align: left; color: black">Nama</td>
                        <td width="2%" style="text-align: left; color: black">:</td>
                        <td width="200%" style="text-align: left; color:black"value="{{$data->havewaris->nama_ahli_waris}}">{{$data->havewaris->nama_ahli_waris}}</td>
                    </tr>
                    <tr>
                        <td width="25%" style="text-align: left;color: black">Pekerjaan</td>
                        <td width="2%" style="text-align: left;color: black">:</td>
                        <td style="text-align: left; color:black"value="{{$data->havewaris->haveSuplier->nama_pekerjaan}}">{{$data->havewaris->haveSuplier->nama_pekerjaan}}</td>
                    </tr>
                    <tr>
                        <td width="50%" style="text-align: left;color: black">Alamat </td>
                        <td width="25%" style="text-align: left;color: black">jln</td>
                        <td width="2%" style="text-align: left;color: black ">:</td>
                        <td width="250px" style="text-align: left; color:black"value="{{$data->havewaris->jalan_gang}}">{{$data->havewaris->jalan_gang}}</td> 
                    </tr>
                    <tr>
                    <td></td>
                    <td width="25%" style="text-align: left;color: black">RT</td>
                        <td width="2%" style="text-align: left;color: black">:</td>
                        <td style="text-align: left; color:black"value="{{$data->havewaris->RT}}">{{$data->havewaris->RT}}</td>
                    </tr>
                    <tr>
                    <td></td>
                    <td width="25%" style="text-align: left;color: black">RW</td>
                        <td width="2%" style="text-align: left;color: black">:</td>
                        <td style="text-align: left; color:black"value="{{$data->havewaris->RW}}">{{$data->havewaris->RW}}</td>
                    </tr>
                    <tr>
                    <td>
                    <td width="25%" style="text-align: left;color: black">Telpon</td>
                        <td width="2%" style="text-align: left;color: black">:</td>
                        <td style="text-align: left; color:black"value="{{$data->havewaris->telepon}}">{{$data->havewaris->telepon}}</td>
                    </td>
                    </tr>
                    <tr>
                        <td width="25%" style="text-align: left;color: black"> </td>
                        <td width="25%" style="text-align: left;color: black">Kelurahan/Desa</td>
                        <td width="2%" style="text-align: left;color: black">:</td>
                        <td style="text-align: left; color:black"value="{{$data->havewaris->haveVillage->nama}}">{{$data->havewaris->haveVillage->nama}}</td>
                    </tr>
                    <tr>
	                    <td width="25%" style="text-align: left;color: black"> </td>
                        <td width="25%" style="text-align: left;color: black">Kecamatan</td>
                        <td width="2%" style="text-align: left;color: black">:</td>
                        <td style="text-align: left; color:black"value="{{$data->havewaris->haveDistrict->nama}}">{{$data->havewaris->haveDistrict->nama}}</td>
                    </tr>
                    <tr>
                        <td width="25%" style="text-align: left;color: black"> </td>
                        <td width="25%" style="text-align: left;color: black">Kabupaten/kota</td>
                        <td width="2%" style="text-align: left;color: black">:</td>
                        <td style="text-align: left; color:black"value="{{$data->havewaris->haveCity->nama}}">{{$data->havewaris->haveCity->nama}}</td>
                    </tr>
					<tr>
                        <td width="25%" style="text-align: left;color: black">Biaya</td>
                       
                        <td style="text-align: left; color:black">Rp.1.200.000</td>
                    </tr>
					<tr>
                        <td width="25%" style="text-align: left;color: black">per_tahun</td>
                       
                        <td style="text-align: left; color:black">Rp.20.000</td>
                    </tr>
                </tbody>
            </table><br>
            <p>
        </div>
        </div><br>
        <table border="0" cellspacing="0" cellpadding="0">
            <tbody>

                <tr>
                    <td style="background: #EEEEEE; border-bottom: 1px solid #FFFFFF; " colspan="2" id="left">
                        <br>
                    </td>
                    <td style="background: #EEEEEE; border-bottom: 1px solid #FFFFFF; " colspan="3"><br>
                    </td>
                </tr>

            </tbody>
        </table>
        <div id="terbilang"></div>
        <div id="terimkasih"> <br>
            <div id="notices">
                <div class="notice">
                </div>
            </div>
            <br>
            <div id="invoice">
                <div>An.KEPALA DINAS PENATAAN RUANG</div>
                <div>KOTA BANDUNG</div>
                <div>Kepala UPT Pemakaman</div>
                <div>Wilayah 1</div><br><br><br>
				<div>Drs.BAMBANG SURYAMAN, ST,Msi</div>
				<div>PEMBINA,IV/a</div>
				<div>NIP.196405201990031012</div>

            </div>
    </main>
</body>

</html>