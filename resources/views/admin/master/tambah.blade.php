@extends('admin.layouts.master')
@section('content')
<div class="col-12 col-md-12 col-lg-12">
    <div class="card">
        <div class="card-body">
            <div class="card-header">
                <h2>Tambah Data</h2>
            </div>

            <form action="{{route('postdata_master')}}" method="post">
                @csrf
                <div class="form-group"><br>
                   No makam <br><br>
                    <input type="number" name="no_makam" class="form-control" id="no_makam">
                <div class="form-group"><br>
                    Kode Blok <br><br>
                    <input type="text" name="kode_blok" class="form-control" id="kode_blok">


                    <div class="form-group"><br><br>
                        Lokasi <br><br>
                        <input type="text" name="lokasi" class="form-control" id="lokasi">


                    </div>

                    <div class="form-group">
                        Kelas <br><br>
                        <input type="text" name="kelas" class="form-control" id="kelas">


                    </div>

                    <button type="submit" name="Tambah" class="btn btn-primary float-right"> Simpan Data</button>
                    &nbsp<button type="submit" name="Tambah" class="btn btn-warning float-right"> Kembali</button>
            </form>
        </div>
    </div>
</div>

@endsection
