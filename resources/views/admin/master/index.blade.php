@extends('admin.layouts.master')
@section('content')
<div class="col-12 col-md-12 col-lg-12">
    <div class="card">
        <div class="card-header">
            @if(session()->has('message'))
            {{session()->get('message')}}
            @endif
            <div><a href="{{route('tambah_master')}}" class="btn btn-primary">Tambah Data</a></div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered table-md">
                    <tbody>
                        <tr>
                            <th>NO</th>
                            <th>id makam </th>
                            <th>No Makam</th>
                            <th>Blok</th>
                            <th>Lokasi</th>
                            <th>Kelas</th>
                            <th>Action</th>
                        </tr>
                        @php
                        $i=1;
                        @endphp
                        @foreach($data as $row)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$row->id_makam}}</td>
                            <td>{{$row->no_makam}}</td>
                            <td>{{$row->kode_blok}}</td>
                            <td>{{$row->lokasi}}</td>
                            <td>{{$row->kelas}}</td>
                            <td><a href="{{route('edit_master',$row->id_makam)}}" class="btn btn-success">Edit</a>&nbsp 
                            <a href="{{route('delete_master',$row->id_makam)}}" class="btn btn-danger">Delete</a></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
