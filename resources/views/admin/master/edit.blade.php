@extends('admin.layouts.master')
@section('content')
<div class="col-12 col-md-12 col-lg-12">
    <div class="card">
        <div class="card-body">
            <div class="card-header">
                <h2>Edit Data</h2>
            </div>

            <form action="{{route('update_master',$data->id_makam)}}" method="post">
                @csrf
                <div class="form-group"><br>
                    No Makam <br><br>
                    <input type="number" name="no_makam" class="form-control" id="no_makam" value="{{$data->no_makam}}">


                    <div class="form-group"><br><br>
                        Kode Blok <br><br>
                        <input type="text" name="kode_blok" class="form-control" id="kode_blok" value="{{$data->kode_blok}}">


                    </div>

                    <div class="form-group">
                        lokasi <br><br>
                        <input type="text" name="lokasi" class="form-control" id="lokasi" value="{{$data->lokasi}}">


                    </div>

                    <div class="form-group">
                        kelas <br><br>
                        <input type="text" name="kelas" class="form-control" id="kelas" value="{{$data->kelas}}">

                    </div>
                    <button type="submit" name="Tambah" class="btn btn-primary float-right"> Simpan Data</button> &nbsp<button type="submit" name="Tambah" class="btn btn-warning float-right">  Kembali</button>
            </form>
        </div>
    </div>
</div>

@endsection
