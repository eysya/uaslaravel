@extends('admin.layouts.master')
@section('content')
<div class="col-12 col-md-12 col-lg-12">
    <div class="card">
    @if ( session('success'))
            <div class="alert alert-success">
              {{ session('success') }}
            </div>
          @endif
        <div class="card-header">
            <form class="form-inline mr-auto" action="{{route('cari_data')}}">
                <ul class="navbar-nav mr-3">
                    <li><a href="#" data-toggle="search" class="nav-link nav-link-lg d-sm-none"><i
                                class="fas fa-search"></i></a></li>
                </ul>
                <div class="search-element">
                    <input class="form-control" type="search" placeholder="Search" aria-label="Search" name="Search" data-width="250">
                    <button class="btn" type="submit"><i class="fas fa-search"></i></button>
                    <div class="search-backdrop"></div>
                </div>
            </form>

            
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered table-md">
                    <tbody>
                        <tr>
                            <th>NO</th>
                            <th>Nama mendiang</th>
                            <th>Bin/Binti</th>
                            <th>Jenis Kelamin</th>
                            <th>meninggal</th>
                            <th>Dimakamkan</th>
                            <th>Ahliwaris</th>
                            <th>pekerjaan</th>

                            <th>Action</th>
                        </tr>
                        @php
                        $i=1;
                        @endphp
                        @foreach($data as $row)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$row->nama_mendiang}}</td>
                            <td>{{$row->Bin_Binti}}</td>
                            <td>{{$row->jenis_kelamin}}</td>
                            <td>{{$row->tanggal_meninggal}}</td>
                            <td>{{$row->tanggal_dimakamkan}}</td>
                            <td>{{$row->havewaris->nama_ahli_waris}}</td>
                            <td>{{$row->havewaris->haveSuplier->nama_pekerjaan}}</td>
                            <td><a href="{{route('cek_detail',$row->id_pemakaman)}}"
                                    class="btn btn-success">Detail</a>&nbsp <a href="{{route('cetak_kartu',$row->id_pemakaman)}}"
                                    class="btn btn-primary">Cetak</a></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div>
@endsection
