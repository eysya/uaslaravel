@extends('admin.layouts.master')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10">
            <div class="card">
                <div class="card-body">
                    <div class="card-header">
                        <h3>DATA YANG MENINGGAL</h3>
                    </div>
                    <div class="col-md-12">
                    <form action="">
                         <table>
                            <tr>
                                <td width="30%">Nama</td>
                                <td>:</td>
                                <td value="{{$data->nama_mendiang}}">{{$data->nama_mendiang}}</td>
                            </tr>
                            <tr>
                                <td>Bin/Binti</td>
                                <td>:</td>
                                <td value="{{$data->Bin_Binti}}">{{$data->Bin_Binti}}</td>
                            </tr>
                            <tr>
                                <td>Jenis Kelamin</td>
                                <td>:</td>
                                <td value="{{$data->jenis_kelamin}}">{{$data->jenis_kelamin}}</td>
                            </tr>
                            <tr>
                                <td>Tanggal Meninggal</td>
                                <td>:</td>
                                <td value="{{$data->tanggal_meninggal}}">{{$data->tanggal_meninggal}}</td>
                            </tr>
                            <tr>
                                <td>Tanggal Dimakamkanl</td>
                                <td>:</td>
                                <td value="{{$data->tanggal_dimakamkan}}">{{$data->tanggal_dimakamkan}}</td>
                            </tr>
                            <tr>
                                <td>Lokasi Pemakaman</td>
                                <td>
                                    <label for=""> TPU </label>
                                </td>
                                <td>:</td>
                                <td value="{{$data->havemaster->lokasi}}">{{$data->havemaster->lokasi}}</td>
                                <td>
                                    <label for=""> Blok </label>
                                </td>
                                <td class="mr-2">:</td>
                                <td value="{{$data->havemaster->kode_blok}}">{{$data->havemaster->kode_blok}}</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <label for=""> Kelas </label>
                                </td>
                                <td>:</td>
                                <td value="{{$data->havemaster->kelas}}">{{$data->havemaster->kelas}}</td>
                                <td>
                                    <label for=""> Nomor </label>
                                </td>
                                <td>:</td>
                                <td value="{{$data->havemaster->no_makam}}">{{$data->havemaster->no_makam}}</td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-12">
                        <div class="card-header">
                            <h3>AHLI WARIS/PENANGGUNG JAWAB</h3>
                        </div>
                        <table>
                            <tr>
                                <td width="30%">Nama Ahli Waris</td>
                                <td>:</td>
                                <td value="{{$data->havewaris->nama_ahli_waris}}">{{$data->havewaris->nama_ahli_waris}}</td>
                            </tr>
                            <tr>
                                <td>Pekerjaan</td>
                                <td>:</td>
                                <td value="{{$data->havewaris->haveSuplier->nama_pekerjaan}}">{{$data->havewaris->haveSuplier->nama_pekerjaan}}</td>
                            </tr>
                            <tr>
                                <td>Alamat</td>
                                <td>
                                    <label for=""> Jln </label>
                                </td>
                                <td>:</td>
                                <td value="{{$data->havewaris->jalan_gang}}">{{$data->havewaris->jalan_gang}}</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <label for=""> RT </label>
                                </td>
                                <td>:</td>
                                <td value="{{$data->havewaris->RT}}">{{$data->havewaris->RT}}</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <label for=""> RW </label>
                                </td>
                                <td>:</td>
                                <td value="{{$data->havewaris->RW}}">{{$data->havewaris->RW}}</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <label for=""> Telepon </label>
                                </td>
                                <td>:</td>
                                <td value="{{$data->havewaris->telepon}}">{{$data->havewaris->telepon}}</td>
                            </tr>

                            <tr>
                                <td></td>
                                <td>
                                    <label for=""> Kelurahan/desa </label>
                                </td>
                                <td>:</td>
                                <td value="{{$data->havewaris->haveVillage->nama}}">{{$data->havewaris->haveVillage->nama}}</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <label for=""> Kecamatan </label>
                                </td>
                                <td>:</td>
                                <td value="{{$data->havewaris->haveDistrict->nama}}">{{$data->havewaris->haveDistrict->nama}}</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <label for=""> Kabupaten/kota </label>
                                </td>
                                <td>:</td>
                                <td value="{{$data->havewaris->haveCity->nama}}">{{$data->havewaris->haveCity->nama}}</td>
                            </tr>
                            


                        </table>
                                                
                        </form>
                    </div>
                    

                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
