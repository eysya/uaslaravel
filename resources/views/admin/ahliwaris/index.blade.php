@extends('admin.layouts.master')
@section('content')
<div class="col-12 col-md-12 col-lg-12">
    <div class="card">
        <div class="card-header">
            @if(session()->has('message'))
            {{session()->get('message')}}
            @endif
            <form class="form-inline mr-auto" action="{{route('Serch_waris')}}">
                <ul class="navbar-nav mr-3">
                    <li><a href="#" data-toggle="search" class="nav-link nav-link-lg d-sm-none"><i
                                class="fas fa-search"></i></a></li>
                </ul>
                <div class="search-element">
                    <input class="form-control" type="search" placeholder="Search" aria-label="Search" name="keyword" data-width="250">
                    <button class="btn" type="submit"><i class="fas fa-search"></i></button>
                    <div class="search-backdrop"></div>
                </div>
            </form>
        
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered table-md">
                    <tbody>
                        <tr>
                            <th>NO</th>
                            <th>id Ahli waris </th>
                            <th>Nama ahli waris </th>
                            <th>umur </th>
                            <th>telepon </th>
                            <th>Pekerjaan</th>
                            <th>jalan /gang</th>
                            <th>No</th>
                            <th>RT</th>
                            <th>RW</th>
                            <th>kabupaten/kota</th>
                            <th>kecmatan</th>
                            <th>Desa</th>
                            <th>Action</th>
                        </tr>
                        @php
                        $i=1;
                        @endphp
                        @foreach($data as $row)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$row->id_ahli_waris}}</td>
                            <td>{{$row->nama_ahli_waris}}</td>
                            <td>{{$row->umur}}</td>
                            <td>{{$row->telepon}}</td>
                            <td>{{$row->haveSuplier->nama_pekerjaan}}</td>
                            <td>{{$row->jalan_gang}}</td>
                            <td>{{$row->nomor}}</td>
                            <td>{{$row->RT}}</td>
                            <td>{{$row->RW}}</td>
                            <td>{{$row->haveCity->nama}}</td>
                            <td>{{$row->haveDistrict->nama}}</td>
                            <td>{{$row->havevillage->nama}}</td>
                            <td><a href="{{route('edit_waris',$row->id_ahli_waris)}}" class="btn btn-success" >Edit</a>&nbsp 
                            <a href="{{route('delete_waris',$row->id_ahli_waris)}}" class="btn btn-danger" >Delete</a></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <div class="card-footer text-right">
            <nav class="d-inline-block">
                <ul class="pagination mb-0">
                    <li class="page-item disabled">
                        <a class="page-link" href="#" tabindex="-1"><i class="fas fa-chevron-left"></i></a>
                    </li>
                    <li class="page-item active"><a class="page-link" href="#">1 <span
                                class="sr-only">(current)</span></a></li>
                    <li class="page-item">
                        <a class="page-link" href="#">2</a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item">
                        <a class="page-link" href="#"><i class="fas fa-chevron-right"></i></a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</div>
@endsection
