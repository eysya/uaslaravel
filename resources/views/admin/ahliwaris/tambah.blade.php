@extends('admin.layouts.master')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10">
            <div class="card">
                <div class="card-body">
                @if ( session('success'))
            <div class="alert alert-success">
              {{ session('success') }}
            </div>
          @endif
                    <div class="card-header">
                        <h3>Daftar Ahli Waris</h3>
                    </div>
                    <div class="col-md-12">
                        <form action="{{route('waris_postdata')}}" method="post">
                            @csrf
                            <table>
                                <tr>
                                    <td width="30%">Nama Ahli Waris</td>
                                    <td><input type="text" class="form-control" name="nama_ahli_waris"></input></td>
                                </tr>
                                <tr>
                                    <td>Umur</td>
                                    <td><input type="number" class="form-control" name="umur"></input></td>
                                </tr>
                                <tr>
                                    <td>pekerjaan</td>
                                    <td>
                                        <div class="form-group">

                                            <select class="form-control" name="id_pekerjaan">
                                                <option>--pilih Pekerjaan--</option>
                                                @foreach($job as $row)
                                                <option value="{{$row->id_pekerjaan}}">{{$row->id_pekerjaan}} -
                                                    {{$row->nama_pekerjaan}}
                                                </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Alamat</td>
                                    <td>
                                        <label for=""> jln </label>
                                    </td>
                                    <td><input type="text" class="form-control" name="jalan_gang"></input></td>
                                    <td></td>
                                    <td>
                                        <label for=""> nomor </label>
                                    </td>
                                    <td><input type="number" class="form-control" name="nomor"></input></td>

                                </tr>

                                <tr>
                                    <td></td>
                                    <td>
                                        <label for=""> RT </label>
                                    </td>
                                    <td><input type="text" class="form-control" name="RT"></input></td>
                                    <td></td>
                                    <td>
                                        <label for=""> RW </label>
                                    </td>
                                    <td><input type="text" class="form-control" name="RW"></input></td>

                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <label for=""> Kelurahan/desa </label>
                                    </td>
                                    <td>
                                        <div class="form-group">

                                            <select class="form-control" name="id_village">
                                                <option>--pilih Desa --</option>
                                                @foreach($desa as $row)
                                                <option value="{{$row->id_village}}">{{$row->id_village}} -
                                                    {{$row->nama}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </td>
                                    <td></td>
                                    <td>
                                        <label for=""> Kecamatan </label>
                                    </td>
                                    <td>
                                        <div class="form-group">

                                            <select class="form-control" name="id_districts">
                                                <option>--pilih kecamatan --</option>
                                                @foreach($kec as $row)
                                                <option value="{{$row->id_districts}}">{{$row->id_districts}} -
                                                    {{$row->nama}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </td>

                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <label for=""> Kabupaten/kota </label>
                                    </td>
                                    <td>
                                        <div class="form-group">

                                            <select class="form-control" name="id_city">
                                                <option>--pilih Kabupaten/kota--</option>
                                                @foreach($city as $row)
                                                <option value="{{$row->id_city}}">{{$row->id_city}} - {{$row->nama}}
                                                </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>No telp</td>
                                    <td><input type="text" class="form-control" name="telepon"></input></td>
                                </tr>

                            </table>
                            <button class="btn-succes">Next</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
