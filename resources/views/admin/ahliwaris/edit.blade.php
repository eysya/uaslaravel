@extends('admin.layouts.master')
@section('content')
<div class="col-12 col-md-12 col-lg-12">
    <div class="card">
        <div class="card-body">
            <div class="card-header">
                <h2>Tambah Data</h2>
            </div>

            <form action="{{route('update_waris',$data->id_ahli_waris)}}" method="post">
                @csrf
                <div class="form-group"><br>
                    Nama Penanggung jawab<br><br>
                    <input type="text" name="nama_ahli_waris" class="form-control" id="nama_ahli_waris"value="{{$data->nama_ahli_waris}}">


                    <div class="form-group"><br><br>
                        Umur  <br><br>
                        <input type="number" name="umur" class="form-control" id="umur" value="{{$data->umur}}">
                    </div>
                    <div class="form-group"><br><br>
                        Telepon  <br><br>
                        <input type="text" name="telepon" class="form-control" id="telepon" value="{{$data->telepon}}">
                    </div>
                    <div class="form-group">
                      <label> Pekerjaan</label>
                      <select class="form-control" name="id_pekerjaan" value="{{$data->id_pekerjaan}}">
                        <option value="{{$data->id_pekerjaan}}" >{{$data->haveSuplier->nama_pekerjaan}}</option>
                        @foreach($job as $row)
                        <option value="{{$row->id_pekerjaan}}">{{$row->id_pekerjaan}} - {{$row->nama_pekerjaan}}</option>
                        @endforeach
                      </select>
                    </div>
                    <div class="form-group"><br><br>
                        jalan  <br><br>
                        <input type="text" name="jalan_gang" class="form-control" id="jalan_gang"value="{{$data->jalan_gang}}">
                    </div>
                    <div class="form-group"><br><br>
                        NO  <br><br>
                        <input type="number" name="nomor" class="form-control" id="nomor"value="{{$data->nomor}}">
                    </div>
                    <div class="form-group"><br><br>
                        RT  <br><br>
                        <input type="text" name="RT" class="form-control" id="RT"value="{{$data->RT}}">
                    </div>
                    <div class="form-group"><br><br>
                        RW  <br><br>
                        <input type="text" name="RW" class="form-control" id="RW"value="{{$data->RW}}">
                    </div>
                    <div class="form-group">
                      <label> Kabupaten / kota</label>
                      <select class="form-control" name="id_city" value="{{$data->id_city}}">
                        <option value="{{$data->id_city}}">{{$data->haveCity->nama}}</option>
                        @foreach($city as $row)
                        <option value="{{$row->id_city}}">{{$row->id_city}} - {{$row->nama}}</option>
                        @endforeach
                      </select>
                    </div>
                    <div class="form-group">
                      <label> Kecamatan</label>
                      <select class="form-control" name="id_districts" value="{{$data->id_districts}}">
                        <option value="{{$data->id_districts}}">{{$data->haveDistrict->nama}}</option>
                        @foreach($kec as $row)
                        <option value="{{$row->id_districts}}">{{$row->id_districts}} - {{$row->nama}}</option>
                        @endforeach
                      </select>
                    </div>
                    <div class="form-group">
                      <label> Desa</label>
                      <select class="form-control" name="id_village" value="{{$data->id_village}}">
                        <option value="{{$data->id_village}}">{{$data->havevillage->nama}}</option>
                        @foreach($desa as $row)
                        <option value="{{$row->id_village}}">{{$row->id_village}} - {{$row->nama}}</option>
                        @endforeach
                      </select>
                    </div>

                    <button type="submit" name="Tambah" class="btn btn-primary float-right"> Simpan Data</button>
                    &nbsp<button type="submit" name="Tambah" class="btn btn-warning float-right"> Kembali</button>
            </form>
        </div>
    </div>
</div>

@endsection
