@extends('admin.layouts.master')
@section('content')
<div class="col-12 col-md-12 col-lg-12">
    <div class="card">
        @if(session()->has('message'))
        {{session()->get('message')}}
        @endif
        <div class="card-header">
            <form class="form-inline mr-auto" action="{{route('Serch_mendiang')}}">
                <ul class="navbar-nav mr-3">
                    <li><a href="#" data-toggle="search" class="nav-link nav-link-lg d-sm-none"><i
                                class="fas fa-search"></i></a></li>
                </ul>
                <div class="search-element">
                    <input class="form-control" type="search" placeholder="Search" aria-label="Search" name="keyword" data-width="250">
                    <button class="btn" type="submit"><i class="fas fa-search"></i></button>
                    <div class="search-backdrop"></div>
                </div>
            </form>

           
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered table-md">
                    <tbody>
                        <tr>
                            <th>NO</th>
                            <th>Nama mendiang</th>
                            <th>Bin/Binti</th>
                            <th>Jenis Kelamin</th>
                            <th>tempat Lahir</th>
                            <th>tanggal lahir </th>
                            <th>meninggal</th>
                            <th>Dimakamkan</th>
                            <th>jalan/gang</th>
                            <th>Nomor_rumah</th>
                            <th>RT</th>
                            <th>RW</th>
                            <th>Desa/Kel*)</th>
                            <th>Kecamatan</th>
                            <th>Kab/Kota</th>
                            <th>fotocopy KTP</th>
                            <th>surat dari rumah sakit</th>
                            <th>surat pengantar RT/RW</th>
                            <th>Lokasi</th>
                            <th>blok</th>
                            <th>Nomor</th>
                            <th>Kelas</th>
                            <th>Ahliwaris</th>
                            <th>Action</th>
                        </tr>
                        @php
                        $i=1;
                        @endphp
                        @foreach($data as $row)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$row->nama_mendiang}}</td>
                            <td>{{$row->Bin_Binti}}</td>
                            <td>{{$row->jenis_kelamin}}</td>
                            <td>{{$row->tempat}}</td>
                            <td>{{$row->tanggal_lahir}}</td>
                            <td>{{$row->tanggal_meninggal}}</td>
                            <td>{{$row->tanggal_dimakamkan}}</td>
                            <td>{{$row->jalan_gang}}</td>
                            <td>{{$row->nomor_rumah}}</td>
                            <td>{{$row->RT}}</td>
                            <td>{{$row->RW}}</td>
                            <td>{{$row->haveVillage->nama}}</td>
                            <td>{{$row->haveDistrict->nama}}</td>
                            <td>{{$row->haveCity->nama}}</td>
                            <td>{{$row->fc_ktp}}</td>
                            <td>{{$row->surat_kematian}}</td>
                            <td>{{$row->surat_pengantar}}</td>
                            <td>{{$row->havemaster->lokasi}}</td>
                            <td>{{$row->havemaster->kode_blok}}</td>
                            <td>{{$row->havemaster->no_makam}}</td>
                            <td>{{$row->havemaster->kelas}}</td>
                            <td>{{$row->havewaris->nama_ahli_waris}}</td>
                            <td><a href="{{route('edit_mendiang',$row->id_pemakaman)}}"
                                    class="btn btn-success">Edit</a>&nbsp
                                <a href="{{route('deletemendiang',$row->id_pemakaman)}}"
                                    class="btn btn-danger">Delete</a></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <div class="card-footer text-right">
            <nav class="d-inline-block">
                <ul class="pagination mb-0">
                    <li class="page-item disabled">
                        <a class="page-link" href="#" tabindex="-1"><i class="fas fa-chevron-left"></i></a>
                    </li>
                    <li class="page-item active"><a class="page-link" href="#">1 <span
                                class="sr-only">(current)</span></a></li>
                    <li class="page-item">
                        <a class="page-link" href="#">2</a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item">
                        <a class="page-link" href="#"><i class="fas fa-chevron-right"></i></a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</div>
@endsection
