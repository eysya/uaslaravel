@extends('admin.layouts.master')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10">
            <div class="card">
                <div class="card-body">
                @if ( session('success'))
            <div class="alert alert-success">
              {{ session('success') }}
            </div>
          @endif
                    <div class="card-header">
                        <h3>Daftar mendiang</h3>
                    </div>
                    <div class="col-md-12">
                        <form action="{{route('post_mendiang')}}" method="post">
                        @csrf
                            <table>
                                <tr>
                                    <td width="30%">Nama Mendiang</td>
                                    <td><input type="text" class="form-control" name="nama_mendiang"></input></td>
                                    <td>id_makam</td>
                                    <td>
                                        <div class="form-group">

                                            <select class="form-control" name="id_makam">
                                                <option>--pilih id makam --</option>
                                                @foreach($mkm as $row)
                                                <option value="{{$row->id_makam}}">{{$row->id_makam}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </td>

                                </tr>
                                <tr>
                                    <td>Bin/Binti</td>
                                    <td><input type="text" class="form-control" name="Bin_Binti"></input></td>
                                    <td>Jenis Kelamin</td>
                                    <td>
                                        <div class="form-group">

                                            <select class="form-control" name="jenis_kelamin">
                                                <option>--pilih Jenis kelamin--</option>

                                                <option value="Laki-Laki">Laki-Laki</option>
                                                <option value="Perempuan">Perempuan</option>

                                            </select>
                                        </div>
                                    </td>

                                </tr>
                                <tr>
                                    <td>Temapt</td>
                                    <td><input type="text" class="form-control" name="tempat"></input></td>
                                    <td>
                                        <label for=""> Tanggal Lahir </label>
                                    </td>
                                    <td><input type="date" class="form-control"  name="tanggal_lahir"></input></td>

                                </tr>
                                <tr>
                                    <td>Tanggal Meninggal</td>
                                    <td><input type="date" class="form-control" name="tanggal_meninggal"></input></td>
                                </tr>
                                <tr>
                                    <td>Tanggal Dimakamkan</td>
                                    <td><input type="date" class="form-control"  name="tanggal_dimakamkan"></input></td>
                                </tr>
                                <tr>
                                    <td>Alamat</td>
                                    <td>
                                        <label for=""> jln </label>
                                    </td>
                                    <td><input type="text" class="form-control" name="jalan_gang"></input></td>
                                    <td>
                                        <label for=""> nomor </label>
                                    </td>
                                    <td><input type="text" class="form-control" name="nomor_rumah"></input></td>

                                </tr>

                                <tr>
                                    <td></td>
                                    <td>
                                        <label for=""> RT </label>
                                    </td>
                                    <td><input type="text" class="form-control" name="RT"></input></td>
                                    <td>
                                        <label for=""> RW </label>
                                    </td>
                                    <td><input type="text" class="form-control"  name="RW"></input></td>

                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <label for=""> Kelurahan/desa </label>
                                    </td>
                                    <td>
                                        <div class="form-group">

                                            <select class="form-control" name="id_village">
                                                <option>--pilih Kel/Desa --</option>
                                                @foreach($desa as $row)
                                                <option value="{{$row->id_village}}">{{$row->id_village}} - {{$row->nama}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </td>
                                    <td>
                                        <label for=""> Kecamatan </label>
                                    </td>
                                    <td>
                                        <div class="form-group">

                                            <select class="form-control" name="id_districts">
                                                <option>--pilih kecamatan --</option>
                                                @foreach($kec as $row)
                                                <option value="{{$row->id_districts}}">{{$row->id_districts}} - {{$row->nama}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>kabupaten/Kota</td>
                                    <td>
                                        <div class="form-group">

                                            <select class="form-control" name="id_city">
                                                <option>--pilih kecamatan --</option>
                                                @foreach($city as $row)
                                                <option value="{{$row->id_city}}">{{$row->id_city}} - {{$row->nama}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Ahli Waris </td>
                                    <td>
                                        <div class="form-group">

                                            <select class="form-control" name="id_ahli_waris">
                                                <option>--pilih Ahli Waris --</option>
                                                @foreach($ahli as $row)
                                                <option value="{{$row->id_ahli_waris}}">{{$row->id_ahli_waris}} - {{$row->nama_ahli_waris}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td>Lokasi Pemakaman TPU</td>
                                    <td>
                                        <div class="form-group">

                                            <select class="form-control" name="lokasi">
                                                <option>--pilih Lokasi --</option>
                                                @foreach($mkm as $row)
                                                <option value="{{$row->id_makam}}">{{$row->id_makam}} - {{$row->lokasi}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <label for=""> Blok </label>
                                    </td>
                                    <td>
                                        <div class="form-group">

                                            <select class="form-control" name="blok">
                                                <option>--pilih Blok --</option>
                                                @foreach($mkm as $row)
                                                <option value="{{$row->id_makam}}">{{$row->id_makam}} - {{$row->kode_blok}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </td>
                                    <td>
                                        <label for="">Kelas </label>
                                    </td>
                                    <td>
                                        <div class="form-group">

                                            <select class="form-control" name="kelas">
                                                <option>--pilih Kelas --</option>
                                                @foreach($mkm as $row)
                                                <option value="{{$row->id_makam}}">{{$row->id_makam}} - {{$row->kelas}}</option>
                                                @endforeach
                                            </select>
                                        </div></input>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <label for=""> Nomor </label>
                                    </td>
                                    <td>
                                        <div class="form-group">

                                            <select class="form-control" name="nomor">
                                                <option>--pilih nomor --</option>
                                                @foreach($mkm as $row)
                                                <option value="{{$row->no_makam}}">{{$row->id_makam}} - {{$row->no_makam}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </td>

                                </tr>
                            </table>
                            <table>
                                <tr>
                                    <label for="">
                                        <h2>Lampiran persyaratan</h2>
                                    </label></tr>
                                <tr>
                                    <td>Foto Copy ktp </td>
                                    <td><input type="file" class="form-control" name="fc_ktp"></input></td>
                                </tr>
                                <tr>
                                    <td>Surat Kematian(Lurah/Puskesmas/RS)</td>
                                    <td><input type="file" class="form-control"  name="surat_kematian"></input></td>
                                </tr>
                                <tr>
                                    <td>Surat pengantar(RT,Rw)</td>
                                    <td><input type="file" class="form-control" name="surat_pengantar"></input></td>
                                </tr>
                            </table>

                            <button class="btn btn-succes"> finish</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
