@extends('admin.layouts.master')
@section('content')
<div class="col-12 col-md-12 col-lg-12">
    <div class="card">
        <div class="card-body">
            <div class="card-header">
                <h2>Tambah Data</h2>
            </div>

            <form action="{{route('update_mendiang',$data->id_pemakaman)}}" method="post">
                @csrf
                <div class="form-group"><br>
                   <div class="form-group">
                      <label> Id makam</label>
                      <select class="form-control" name="id_makam" value="{{$data->id_makam}}" >
                        <option>{{$data->id_makam}}</option>
                        @foreach($mkm as $row)
                        <option value="{{$row->id_makam}}">{{$row->id_makam}}</option>
                        @endforeach
                      </select>
                    </div>
                      Nama Mendiang<br><br>
                    <input type="text" name="nama_mendiang" class="form-control" id="nama_mendiang"value="{{$data->nama_mendiang}}">
                    

                    <div class="form-group"><br><br>
                        BIN/BINTI  <br><br>
                        <input type="text" name="Bin_Binti" class="form-control" id="Bin_Binti"value="{{$data->Bin_Binti}}">
                    </div>
                    <div class="form-group">
                      <label> Jenis Kelamin</label>
                      <select class="form-control" name="jenis_kelamin"value="{{$data->jenis_kelamin}}">
                        <option>{{$data->jenis_kelamin}}-</option>
                        <option value="Laki -laki">Laki -Laki</option>
                        <option value="Perempuan">Perempuan</option>
                      </select>
                    </div>
                    <div class="form-group"><br><br>
                        Tempat  <br><br>
                        <input type="text" name="tempat" class="form-control" id="tempat"value="{{$data->tempat}}">
                    </div>
                    <div class="form-group"><br><br>
                        Tanggal Lahir  <br><br>
                        <input type="date" name="tanggal_lahir" class="form-control" id="tanggal_lahir" value="{{$data->tanggal_lahir}}">
                    </div>
                    <div class="form-group"><br><br>
                        Tanggal Meninggal   <br><br>
                        <input type="date" name="tanggal_meninggal" class="form-control" id="tanggal_meninggal"value="{{$data->tanggal_meninggal}}">
                    </div>
                    <div class="form-group"><br><br>
                        Dimakamkan  <br><br>
                        <input type="date" name="tanggal_dimakamkan" class="form-control" id="tanggal_dimakamkan" value="{{$data->tanggal_dimakamkan}}">
                    </div>
                    <div class="form-group"><br><br>
                        jalan/gang  <br><br>
                        <input type="text" name="jalan_gang" class="form-control" id="jalan_gang" value="{{$data->jalan_gang}}">
                    </div>
                    <div class="form-group"><br><br>
                        nomor   <br><br>
                        <input type="number" name="nomor_rumah" class="form-control" id="nomor_rumah" value="{{$data->nomor_rumah}}">
                    </div>
                    <div class="form-group"><br><br>
                        RT  <br><br>
                        <input type="text" name="RT" class="form-control" id="RT" value="{{$data->RT}}">
                    </div>
                    <div class="form-group"><br><br>
                        RW  <br><br>
                        <input type="text" name="RW" class="form-control" id="RW" value="{{$data->RW}}">
                    </div>
                    <div class="form-group">
                      <label> kelurahan/Desa</label>
                      <select class="form-control" name="id_village" value="{{$data->id_village}}" >
                        <option>{{$data->id_village}} - {{$row->nama}}</option>
                        @foreach($desa as $row)
                        <option value="{{$row->id_village}}">{{$row->id_village}} - {{$row->nama}}</option>
                        @endforeach
                      </select>
                    </div>
                    <div class="form-group">
                      <label> kecamatan</label>
                      <select class="form-control" name="id_districts" value="{{$data->id_districts}}" >
                        <option>{{$data->id_districts}} - {{$row->nama}}</option>
                        @foreach($kec as $row)
                        <option value="{{$row->id_districts}}">{{$row->id_districts}} - {{$row->nama}}</option>
                        @endforeach
                      </select>
                    </div>
                    <div class="form-group">
                      <label> kabupaten/kota</label>
                      <select class="form-control" name="id_city" value="{{$data->id_city}}" >
                        <option>{{$data->id_city}} - {{$row->nama}}</option>
                        @foreach($city as $row)
                        <option value="{{$row->id_city}}">{{$row->id_city}} - {{$row->nama}}</option>
                        @endforeach
                      </select>
                    </div>
                    
                    <div class="form-group"><br><br>
                        Foto copy KTP  <br><br>
                        <input type="file" name="fc_ktp" class="form-control" id="fc_ktp"value="{{$data->fc_ktp}}">
                    </div>
                    <div class="form-group"><br><br>
                        Surat Kematian   <br><br>
                        <input type="file" name="surat_kematian" class="form-control" id="surat_kematian"value="{{$data->surat_kematian}}">
                    </div>
                    <div class="form-group"><br><br>
                        Surat Pengantar  <br><br>
                        <input type="file" name="surat_pengantar" class="form-control" id="surat_pengantar"value="{{$data->surat_pengantar}}">
                    </div>
                    <div class="form-group">
                      <label> Nama Ahli Waris</label>
                      <select class="form-control" name="id_ahli_waris"value="{{$data->id_ahli_waris}}">
                    <option value="{{$data->id_ahli_waris}}">{{$data->havewaris->nama_ahli_waris}}</option>
                        @foreach($ahli as $row)
                        <option value="{{$row->id_ahli_waris}}">{{$row->id_ahli_waris}} - {{$row->nama_ahli_waris}}</option>
                        @endforeach
                      </select>
                    </div>
                   

                    <button type="submit" name="Tambah" class="btn btn-primary float-right"> Simpan Data</button>
                    &nbsp<button type="submit" name="Tambah" class="btn btn-warning float-right"> Kembali</button>
            </form>
        </div>
    </div>
</div>

@endsection
