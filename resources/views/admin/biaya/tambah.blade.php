@extends('admin.layouts.master')
@section('content')
<div class="col-12 col-md-12 col-lg-12">
    <div class="card">
        <div class="card-body">
            <div class="card-header">
                <h2>Tambah Data</h2>
            </div>

            <form action="" method="post">
                @csrf
                <div class="form-group"><br>
                   makam<br><br>
                    <input type="number" name="makam_id" class="form-control" id="makam_id">
                <div class="form-group"><br>
                    Biaya<br><br>
                    <input type="double" name="biaya" class="form-control" id="biaya">


                    <div class="form-group"><br><br>
                        Biaya per tahun  <br><br>
                        <input type="double" name="per_tahun" class="form-control" id="per_tahun">


                    </div>
                    <button type="submit" name="Tambah" class="btn btn-primary float-right"> Simpan Data</button>
                    &nbsp<button type="submit" name="Tambah" class="btn btn-warning float-right"> Kembali</button>
            </form>
        </div>
    </div>
</div>

@endsection
