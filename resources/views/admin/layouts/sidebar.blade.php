<div class="main-sidebar">
        <aside id="sidebar-wrapper">
          <div class="sidebar-brand">
            <a href="index.html">Stisla</a>
          </div>
          <div class="sidebar-brand sidebar-brand-sm">
            <a href="index.html">St</a>
          </div>
          <ul class="sidebar-menu">
              <li class="menu-header">Dashboard</li>
              <li class="nav-item dropdown active">
                <a href="" class="nav-link has-dropdown"><i class="fas fa-fire"></i><span>Dashboard</span></a>
                <ul class="dropdown-menu">
                  <li><a class="nav-link" href="{{route('tambahdata_waris')}}">Daftar</a></li>
                  
                </ul>
              </li>
              <li class="nav-item dropdown">
                <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-columns"></i> <span>Data TPU</span></a>
                <ul class="dropdown-menu">
  
                  <li><a class="nav-link" href="{{route('tampil_waris')}}">Data Ahli Waris</a></li>
                  <li><a class="nav-link" href="{{route('tampil_mendiang')}}">Data Mendiang</a></li>
                </ul>
              </li>
        
              <li class="nav-item dropdown">
                <a href="#" class="nav-link has-dropdown"><i class="fas fa-th"></i> <span>Cetak Karu </span></a>
                <ul class="dropdown-menu">
                  <li><a class="nav-link" href="{{route('Tampil_datamakam')}}">Cetak Kartu </a></li>
                </ul>
              </li>
            <div class="mt-4 mb-4 p-3 hide-sidebar-mini">
              <a href="https://getstisla.com/docs" class="btn btn-primary btn-lg btn-block btn-icon-split">
                <i class="fas fa-rocket"></i> Documentation
              </a>
            </div>
        </aside>
      </div>