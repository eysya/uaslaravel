<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AhliwarisModel;
use App\cityModel;
use App\districtModel;
use App\villageModel;
use App\pekerjaanModel;

class AhliWarisController extends Controller
{

    public function index(){
        $data=AhliwarisModel::all()
        ->where('is_active','1');
        return view('admin.ahliwaris.index',compact('data'));
    }

    public function tambahdata(){
        $city =cityModel::all();
        $kec =districtModel::all();
        $desa =villageModel::all();
        $job =pekerjaanModel::all();
         return view('admin.ahliwaris.tambah',compact('city','kec','desa','job'));
    }

    public function postdatawaris(Request $request, AhliwarisModel $AhliwarisModel){
        $simpan = $AhliwarisModel->create([
            'nama_ahli_waris'=>$request->nama_ahli_waris,
            'umur'=>$request->umur,
            'telepon'=>$request->telepon,
            'id_pekerjaan'=>$request->id_pekerjaan,
            'jalan_gang'=>$request->jalan_gang,
            'nomor'=>$request->nomor,
            'RT'=>$request->RT,
            'RW'=>$request->RW,
            'id_city'=>$request->id_city,
            'id_districts'=>$request->id_districts,
            'id_village'=>$request->id_village,
            'is_active'=>1,

        ]);
        if(!$simpan->exists){
            return redirect()->route('tambah_mendiang')->with('error','data gagal disimpan');
        }
        return redirect()->route('tambah_mendiang')->with('success','data berhasil disimpan');
    }
    public function edit($id){
        $city =cityModel::all();
        $kec =districtModel::all();
        $desa =villageModel::all();
        $job =pekerjaanModel::all();
        $data= AhliwarisModel::where('id_ahli_waris',$id)->first();
        return view('admin.ahliwaris.edit',compact('data','city','kec','desa','job'));
    }

    public function updatedata($id ,Request $request, AhliwarisModel $AhliwarisModel){
        $simpan = $AhliwarisModel->where('id_ahli_waris',$id)->update([
            'nama_ahli_waris'=>$request->nama_ahli_waris,
            'umur'=>$request->umur,
            'telepon'=>$request->telepon,
            'id_pekerjaan'=>$request->id_pekerjaan,
            'jalan_gang'=>$request->jalan_gang,
            'nomor'=>$request->nomor,
            'RT'=>$request->RT,
            'RW'=>$request->RW,
            'id_city'=>$request->id_city,
            'id_districts'=>$request->id_districts,
            'id_village'=>$request->id_village,
        ]);
        if(!$simpan){
            return redirect()->route('tampil_waris')->with('error','data gagal disimpan');
        }
        return redirect()->route('tampil_waris')->with('success','data berhasil disimpan');

    }
    public function softdelete($id, AhliwarisModel $AhliwarisModel){
        $simpan  = $AhliwarisModel->where('id_ahli_waris',$id)->update([
            'is_active' => '0',
        ]);
        if(!$simpan){
            return redirect()->route('tampil_waris')->with('error','data gagal disimpan');
        }
        return redirect()->route('tampil_waris')->with('success','data berhasil disimpan');
    }

    public function serchAhliwaris( Request $request , AhliwarisModel $AhliwarisModel ){
        $data = AhliwarisModel::when($request->keyword, function ($query) use ($request){
            $query->where('nama_ahli_waris', 'like', "%{$request->keyword}%");
        })->get()->where('is_active','1');

        return view('admin.ahliwaris.index',compact('data'));
    }
}
