<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BiayaModel;

class BiayaController extends Controller
{
    public function index(){
        $data=BiayaModel::all();
        return view('admin.biaya.index',compact('data'));
    }

    public function tambahdata(){
        return view('admin.biaya.tambah');
    }

    public function postdata(Request $request ,Biayamodel $BiayaModel){
        $simpan = $BiayaModel->insert([
            'biaya'=>$request->biaya,
            'per_tahun'=>$request->per_tahun,
        ]);
        if(!$simpan->exists){
            return redirect()->route('tampil_biaya')->with('error','data gagal disimpan');
        }
        return redirect()->route('tampil_biaya')->with('success','data berhasil disimpan');
    }
}
