<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\User;
use Hash;
use Auth;
use Session;

class authController extends Controller
{
    public function login(){
        return view('admin.auth.login');
    }

    public function sendLoginRequest(Request $request, User $user){
    
        $email = $request->email;
        $password = $request->password;

        $data =  $user::where('email', $email)->first();
        // dd($data);
        if($data){
            if(Hash::check($password, $data->password)){
                Session::put('name', $data->name);
                Session::put('email', $data->email);
                Session::put('login', TRUE);

                return redirect()->route('Tampil_dashboard');
            }else{
                return redirect()->back()->with('error', 'Invalid Email address or Password');
            }
        }else{
            return redirect()->back()->with('error', 'No Data Found');
        }
    }


    public function logout(Request $request)
    {
    if(\Auth::check())
    {
        \Auth::logout();
        $request->session()->invalidate();
    }
    return  redirect()->route('tampil_login');
}

public function register_index(){
    return view('admin.auth.register');
}

public function register_login(Request $request, User $User){
    $simpan= $User->create([
        'Username'=>$request->Username,
        'email'=>$request->email,
        'password'=>$request->bycrpt('password'),
        'is_active'=>1,

    ]);
    if(!$simpan->exists){
        return redirect()->route('tampil_login')->with('error','data gagal disimpan');
    }
    return redirect()->route('tampil_login')->with('success','data berhasil disimpan');

}
}
