<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PemakamanModel;
use App\AhliwarisModel;
use App\BiayaModel;
use App\cityModel;
use App\districtModel;
use App\villageModel;
use App\MasterModel;

class PemakamanController extends Controller
{
    public function index(){
        $data=PemakamanModel::all()
        ->where('is_active','1');
        return view('admin.pemakaman.index',compact('data')); 
    }

    public function tambahdata(){
        $mkm = MasterModel::all();
        $city =cityModel::all();
        $kec =districtModel::all();
        $desa =villageModel::all();
        $ahli =AhliwarisModel::all();
        $biaya =BiayaModel::all();

        return view('admin.pemakaman.tambah',compact('city','kec','desa','ahli','biaya','mkm'));
    }

    public function postdataMendiang(Request $request,PemakamanModel $PemakamanModel ){
        $simpan = $PemakamanModel->create([
            'id_makam'=>$request->id_makam,
            'nama_mendiang'=>$request->nama_mendiang,
            'Bin_Binti'=>$request->Bin_Binti,
            'jenis_kelamin'=>$request->jenis_kelamin,
            'tempat'=>$request->tempat,
            'tanggal_lahir'=>$request->tanggal_lahir,
            'tanggal_meninggal'=>$request->tanggal_meninggal,
            'tanggal_dimakamkan'=>$request->tanggal_dimakamkan,
            'jalan_gang'=>$request->jalan_gang,
            'nomor_rumah'=>$request->nomor_rumah,
            'RT'=>$request->RT,
            'RW'=>$request->RW,
            'id_village'=>$request->id_village,
            'id_districts'=>$request->id_districts,
            'id_city'=>$request->id_city,
            'lokasi'=>$request->lokasi,
            'blok'=>$request->blok,
            'nomor'=>$request->nomor,
            'kelas'=>$request->kelas,
            'fc_ktp'=>$request->fc_ktp,          
            'surat_kematian'=>$request->surat_kematian,
            'surat_pengantar'=>$request->surat_pengantar,
            'id_ahli_waris'=>$request->id_ahli_waris,
            'is_active'=>1,

        ]);
        if(!$simpan->exists){
            return redirect()->route('Tampil_datamakam')->with('error','data gagal disimpan');
        }
        return redirect()->route('Tampil_datamakam')->with('success','data berhasil disimpan');
    }

    public function editmendiang($id,PemakamanModel $PemakamanModel){
        $mkm = MasterModel::all();
        $city =cityModel::all();
        $kec =districtModel::all();
        $desa =villageModel::all();
        $ahli =AhliwarisModel::all();
        $biaya =BiayaModel::all();
        $data = $PemakamanModel->where('id_pemakaman',$id)->first();
        return view('admin.pemakaman.edit',compact('data','mkm','city','kec','desa','ahli','biaya'));
    }

    public function updatemendiang($id,Request $request,PemakamanModel $PemakamanModel ){
        $simpan= $PemakamanModel->where('id_pemakaman',$id)->update([
            'id_makam'=>$request->id_makam,
            'nama_mendiang'=>$request->nama_mendiang,
            'Bin_Binti'=>$request->Bin_Binti,
            'jenis_kelamin'=>$request->jenis_kelamin,
            'tempat'=>$request->tempat,
            'tanggal_lahir'=>$request->tanggal_lahir,
            'tanggal_meninggal'=>$request->tanggal_meninggal,
            'tanggal_dimakamkan'=>$request->tanggal_dimakamkan,
            'jalan_gang'=>$request->jalan_gang,
            'nomor_rumah'=>$request->nomor_rumah,
            'RT'=>$request->RT,
            'RW'=>$request->RW,
            'id_village'=>$request->id_village,
            'id_districts'=>$request->id_districts,
            'id_city'=>$request->id_city,
            'fc_ktp'=>$request->fc_ktp,          
            'surat_kematian'=>$request->surat_kematian,
            'surat_pengantar'=>$request->surat_pengantar,
            'id_ahli_waris'=>$request->id_ahli_waris,


        ]);
        if(!$simpan){
            return redirect()->route('tampil_mendiang')->with('error','data gagal disimpan');
        }
        return redirect()->route('tampil_mendiang')->with('success','data berhasil disimpan');

    }
    public function softdelete($id, PemakamanModel $PemakamanModel){
        $simpan  = $PemakamanModel->where('id_pemakaman',$id)->update([
            'is_active' => '0',
        ]);
        if(!$simpan){
            return redirect()->route('tampil_mendiang')->with('error','data gagal disimpan');
        }
        return redirect()->route('tampil_mendiang')->with('success','data berhasil disimpan');
    }

    public function serchMendiang( Request $request ,PemakamanModel $PemakamanModel ){
        $data = PemakamanModel::when($request->keyword, function ($query) use ($request){
            $query->where('nama_mendiang', 'like', "%{$request->keyword}%")
            ->orWhere('id_city', 'like', "%{$request->keyword}%")
            ->orWhere('id_districts', 'like', "%{$request->keyword}%")
            ->orWhere('id_village', 'like', "%{$request->keyword}%")
            ->orWhere('tanggal_meninggal', 'like', "%{$request->keyword}%");
        })->get()->where('is_active','1');

        return view('admin.pemakaman.index',compact('data'));
    }
    
}
