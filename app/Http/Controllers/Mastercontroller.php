<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MasterModel;

class Mastercontroller extends Controller
{

    public function master(){
        $data=MasterModel::all() 
            ->where('is_active','1');
        return view('admin.master.index',compact('data'));
    }

    public function tambahdata(){
        return view('admin.master.tambah');
    }

    public function postData(Request $request , MasterModel $MasterModel){
        $simpan = $MasterModel->create([
            'no_makam'=>$request->no_makam,
            'kode_blok'=>$request->kode_blok,
            'lokasi'=>$request->lokasi,
            'kelas'=>$request->kelas,
        ]);
        if(!$simpan->exists){
            return redirect()->route('tampil_master')->with('error','data gagal disimpan');
        }
        return redirect()->route('tampil_master')->with('success','data berhasil disimpan');
    }

    public function edit($id){
        $data=MasterModel::where('id_makam',$id)->first();
        return view('admin.master.edit',compact('data'));
    }

    public function update_data($id,Request $request , MasterModel $MasterModel){
        $simpan = $MasterModel->where('id_makam',$id)->update([
            'no_makam'=>$request->no_makam,
            'kode_blok'=>$request->kode_blok,
            'lokasi'=>$request->lokasi,
            'kelas'=>$request->kelas,
        ]);
        if(!$simpan){
            return redirect()->route('tampil_master')->with('error','data gagal disimpan');
        }

        return redirect()->route('tampil_master')->with('success','data berhasil disimpan');
 
    }

    public function softdelete($id, MasterModel $MasterModel){
        $simpan  = $MasterModel->where('id_makam',$id)->update([
            'is_active' => '0',
        ]);
        if(!$simpan){
            return redirect()->route('tampil_master')->with('error','data gagal disimpan');
        }
        return redirect()->route('tampil_master')->with('success','data berhasil disimpan');
    }

}
