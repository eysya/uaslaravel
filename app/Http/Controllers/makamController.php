<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PemakamanModel;
use App\AhliwarisModel;
use App\cityModel;
use App\districtModel;
use App\villageModel;
use App\pekerjaanModel;
use PDF;
use App\User;

class makamController extends Controller
{
   public function index(){
        $data=PemakamanModel::all()
        ->where('is_active','1');;
        return view('admin.makam.makam',compact('data'));
    }

    public function cetakkartu($id){
        $data=PemakamanModel::where('id_pemakaman',$id)->first();

        $pdf = PDF::loadview('pdf.report_data',compact('data'))->setPaper('a4','potrait');

        return  $pdf->stream('cetak_kartu_pdf');
    }
    public function detail($id,PemakamanModel $PemakamanModel){
        $data=$PemakamanModel->where('id_pemakaman',$id)->first();
        return view('pdf.report_data',compact('data'));
    }

    public function caridatamakam( Request $request ,PemakamanModel $PemakamanModel ){
        $data = PemakamanModel::when($request->Search, function ($query) use ($request){
            $query->where('nama_mendiang', 'like', "%{$request->Search}%");
        })->get()->where('is_active','1');

        return view('admin.makam.makam',compact('data'));
    }

    
}
