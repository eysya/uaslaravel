<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterModel extends Model
{
   protected $table ='master_makam';

   protected $fillable = [
      'no_makam','kode_blok','lokasi','kelas','is_active',
  ];
  public function hasManymaster(){
   return $this->hasMany(PemakamanModel::class, 'id_makam','id_makam');
}

}
