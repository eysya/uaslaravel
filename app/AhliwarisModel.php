<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AhliwarisModel extends Model
{
    protected $table= 'ahli_waris';

    protected $fillable = [
        'id_ahli_waris','nama_ahli_waris','umur','telepon','id_pekerjaan','jalan_gang','nomor','RT','RW','id_city','id_districts','id_village','is_active',
    ];
  

    public function haveSuplier(){
        return $this->belongsTo(pekerjaanModel::class,'id_pekerjaan','id_pekerjaan');
    }

    public function haveCity(){
        return $this->belongsTo(cityModel::class,'id_city','id_city');
    }

    public function haveDistrict(){
        return $this->belongsTo(districtModel::class,'id_districts','id_districts');
    }
    public function haveVillage(){
        return $this->belongsTo(villageModel::class,'id_village','id_village');
    }

    public function hasManyAhliwaris(){
        return $this->hasMany(PemakamanModel::class, 'id_ahli_waris','id_ahli_waris');
    }
}
