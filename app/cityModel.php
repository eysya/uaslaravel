<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cityModel extends Model
{
    protected $table= 'city';

    public function hasManyCity(){
        return $this->hasMany(AhliwarisModel::class, 'id_city','id_city');
    }
}
