<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class districtModel extends Model
{
    protected $table= 'districts';

    public function hasManydistrict(){
        return $this->hasMany(AhliwarisModel::class, 'id_districts','id_districts');
    }

}
