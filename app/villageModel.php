<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class villageModel extends Model
{
    protected $table= 'village';

    public function hasManyvillage(){
        return $this->hasMany(AhliwarisModel::class, 'id_village','id_village');
    }
    public function hasManymendiang(){
        return $this->hasMany(PemakamanModel::class, 'id_village','id_village');
    }
}
