<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BiayaModel extends Model
{
    protected $table ='biaya_pemakaman';

    protected $fillable = [
        'kode_biaya','makam_id','biaya','per_tahun','is_active',
    ];
     
    public function hasManyBiaya(){
        return $this->hasMany(PemakamanModel::class, 'kode_biaya','kode_biaya');
    }
}
