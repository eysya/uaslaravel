<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pekerjaanModel extends Model
{
    protected $table ='pekerjaan';


    public function hasManyProduct(){
        return $this->hasMany(AhliwarisModel::class, 'id_pekerjaan','id_pekerjaan');
    }
}
