<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PemakamanModel extends Model
{
    protected $table = 'mendiang';

    protected $fillable = [
        'id_pemakaman ','id_makam','nama_mendiang','Bin_Binti','jenis_kelamin','tempat','tanggal_lahir','tanggal_meninggal','tanggal_dimakamkan','jalan_gang','nomor_rumah',
        'RT','RW','id_village','id_districts','id_city','fc_ktp','surat_kematian','surat_pengantar','lokasi','blok','nomor','kelas','id_ahli_waris','kode_biaya','is_active',
    ];
    public function haveCity(){
        return $this->belongsTo(cityModel::class,'id_city','id_city');
    }

    public function haveDistrict(){
        return $this->belongsTo(districtModel::class,'id_districts','id_districts');
    }
    public function haveVillage(){
        return $this->belongsTo(villageModel::class,'id_village','id_village');
    }
    public function havewaris(){
        return $this->belongsTo(AhliwarisModel::class,'id_ahli_waris','id_ahli_waris');
    }
    public function havebiaya(){
        return $this->belongsTo(BiayaModel::class,'kode_biaya','kode_biaya');
    }
    public function havemaster(){
        return $this->belongsTo(MasterModel::class,'id_makam','id_makam');
    }
}
