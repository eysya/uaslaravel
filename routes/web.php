<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// login
Route::get('login','authController@login')->name('tampil_login');
Route::post('/sendlogin','authController@sendLoginRequest')->name('login_action');
Route::get('logout','authController@logout')->name('logout');
Route::get('register','authController@register_index')->name('Tampil_register');
Route::post('post_register','authController@register_login')->name('register_post');

// dashboard
Route::get('dashboard','DashboardController@index')->name('Tampil_dashboard');

// master
Route::get('master','Mastercontroller@master')->name('tampil_master');
Route::get('tambahdata/master','Mastercontroller@tambahdata')->name('tambah_master');
Route::post('postdata/data','Mastercontroller@postData')->name('postdata_master');
Route::get('edit/master/{id}','Mastercontroller@edit')->name('edit_master');
Route::post('update_data/master/{id}','Mastercontroller@update_data')->name('update_master');
Route::get('delete/master/{id}','Mastercontroller@softdelete')->name('delete_master');
// // Biaya pemakaman
Route::get('biaya','BiayaController@index')->name('tampil_biaya');
Route::get('tambahdata','BiayaController@tambahdata')->name('tambah_biaya');

// Ahli Waris 
Route::get('waris','AhliWarisController@index')->name('tampil_waris');
Route::get('tambah/waris','AhliWarisController@tambahdata')->name('tambahdata_waris');
Route::post('postdatawaris','AhliWarisController@postdatawaris')->name('waris_postdata');
Route::get('edit/waris/{id}','AhliWarisController@edit')->name('edit_waris');
Route::post('updatedata_waris/{id}','AhliWarisController@updatedata')->name('update_waris');
Route::get('deletewaris/{id}','AhliWarisController@softdelete')->name('delete_waris');
Route::get('serchAhliwaris','AhliWarisController@serchAhliwaris')->name('Serch_waris');;

// mendiang 
Route::get('mendiang','PemakamanController@index')->name('tampil_mendiang');
Route::get('tambah_mendiang','PemakamanController@tambahdata')->name('tambah_mendiang');
Route::post('postdata_mendiang','PemakamanController@postdataMendiang')->name('post_mendiang');
Route::get('edit/mendiang/{id}','PemakamanController@editmendiang')->name('edit_mendiang');
Route::post('update/mendiang/{id}','PemakamanController@updatemendiang')->name('update_mendiang');
Route::get('deletemendiang/{id}','PemakamanController@softdelete')->name('deletemendiang');
Route::get('serchMendiang','PemakamanController@serchMendiang')->name('Serch_mendiang');


//datamakam 
Route::get('datamakam','makamController@index')->name('Tampil_datamakam');
Route::get('detail/{id}','makamController@detail')->name('cek_detail');
Route::get('cetakKartu/{id}','makamController@cetakkartu')->name('cetak_kartu');
Route::get('cari/data','makamController@caridatamakam')->name('cari_data');

// daftar
Route::get('daftar','daftarController@index');
